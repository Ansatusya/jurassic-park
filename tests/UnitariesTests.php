<?php
/**
 * Created by PhpStorm.
 * User: Maxime SAUTAREL
 * Date: 22/02/2019
 * Time: 11:16
 */

require_once 'vendor/autoload.php';

use PHPUnit\Framework\TestCase;

class UnitariesTests extends TestCase
{
    public function test_getDino()
    {
        $dino = getDino();
        $this->assertIsArray($dino);
        $this->assertEquals(7, count($dino));
        $this->assertIsString($dino[0]->avatar);
    }

    public function test_getOneDino()
    {
        $dino = getOneDino('dilophosaurus');
        $this->assertIsArray($dino);
        $this->assertEquals(1, count($dino));
        $this->assertIsString($dino[0]->avatar);
    }

    public function test_TopRatedDino()
    {
        $dino = topRatedDino();
        $this->assertIsArray($dino);
        $this->assertEquals(3, count($dino));
    }
}