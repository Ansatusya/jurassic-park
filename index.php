<?php
/**
 * Created by PhpStorm.
 * User: Maxime SAUTAREL
 * Date: 22/02/2019
 * Time: 11:18
 */

require_once "vendor/autoload.php";

$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
    $twig->addFilter(new Twig_Filter('markdown', function ($string) {
        return renderHTMLFromMarkdown($string);
    }));
});

Flight::map('render', function ($template, $data = array()) {
    Flight::view()->display($template, $data);
});

Flight::route('/', function () {
    $data = [
        'dinos' => getDino()
    ];
    Flight::render('dinos.twig', $data);
});

Flight::route('/dinosaur/@dino', function ($dino) {
    $data = [
        'dino' => getOneDino($dino),
        'top' => topRatedDino()
    ];
    Flight::render('details.twig', $data);
});

Flight::start();