<?php
/**
 * Created by PhpStorm.
 * User: Maxime SAUTAREL
 * Date: 22/02/2019
 * Time: 11:13
 */


use Michelf\Markdown;

function getDino()
{
    $response = Requests::get('https://allosaurus.delahayeyourself.info/api/dinosaurs/');
    return json_decode($response->body);
}

function getOneDino($name)
{
    $path = sprintf("https://allosaurus.delahayeyourself.info/api/dinosaurs/%s", $name);
    $response = Requests::get($path);
    return json_decode($response->body);
}

function topRatedDino()
{
    $dinos = getDino();
    $random = array_rand($dinos, 3);
    foreach ($random as $rand) {
        $dino[] = $dinos[$rand];
    }
    return $dino;
}


function renderHTMLFromMarkdown($string_markdown_formatted)
{
    return trim(Markdown::defaultTransform($string_markdown_formatted));
}