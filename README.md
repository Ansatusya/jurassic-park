# ![](assets/img/logo.png)



## How to ?

1. `composer install`
2. `vendor/bin/phpunit tests/`
3. php -S localhost:8000
4. Et go sur [localhost:8000](http://localhost:8000)

## Tests ?

Tests basique de réponse des pages, et des fonction 

 `vendor/bin/phpunit tests/` pour lancer les tests

## Requirements

```
mikecao/flight
symfony/process
guzzlehttp/guzzle
phpunit/phpunit
twig/twig
rmccue/requests
michelf/php-markdown
```

## Authors

> Maxime SAUTAREL